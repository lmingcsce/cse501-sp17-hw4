// Generated from HandlebarsParser.g4 by ANTLR 4.7
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by HandlebarsParser.
function HandlebarsParserListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

HandlebarsParserListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
HandlebarsParserListener.prototype.constructor = HandlebarsParserListener;

// Enter a parse tree produced by HandlebarsParser#document.
HandlebarsParserListener.prototype.enterDocument = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#document.
HandlebarsParserListener.prototype.exitDocument = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#element.
HandlebarsParserListener.prototype.enterElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#element.
HandlebarsParserListener.prototype.exitElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#rawElement.
HandlebarsParserListener.prototype.enterRawElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#rawElement.
HandlebarsParserListener.prototype.exitRawElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#expressionElement.
HandlebarsParserListener.prototype.enterExpressionElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#expressionElement.
HandlebarsParserListener.prototype.exitExpressionElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#literalExpressionElement.
HandlebarsParserListener.prototype.enterLiteralExpressionElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#literalExpressionElement.
HandlebarsParserListener.prototype.exitLiteralExpressionElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#idExpressionElement.
HandlebarsParserListener.prototype.enterIdExpressionElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#idExpressionElement.
HandlebarsParserListener.prototype.exitIdExpressionElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#parenthesizedExpression.
HandlebarsParserListener.prototype.enterParenthesizedExpression = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#parenthesizedExpression.
HandlebarsParserListener.prototype.exitParenthesizedExpression = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#parenthesizedExpressionElement.
HandlebarsParserListener.prototype.enterParenthesizedExpressionElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#parenthesizedExpressionElement.
HandlebarsParserListener.prototype.exitParenthesizedExpressionElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#helperExpressionElement.
HandlebarsParserListener.prototype.enterHelperExpressionElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#helperExpressionElement.
HandlebarsParserListener.prototype.exitHelperExpressionElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#commentElement.
HandlebarsParserListener.prototype.enterCommentElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#commentElement.
HandlebarsParserListener.prototype.exitCommentElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#blockElement.
HandlebarsParserListener.prototype.enterBlockElement = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#blockElement.
HandlebarsParserListener.prototype.exitBlockElement = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#openBlock.
HandlebarsParserListener.prototype.enterOpenBlock = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#openBlock.
HandlebarsParserListener.prototype.exitOpenBlock = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#blockHelper.
HandlebarsParserListener.prototype.enterBlockHelper = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#blockHelper.
HandlebarsParserListener.prototype.exitBlockHelper = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#blockBody.
HandlebarsParserListener.prototype.enterBlockBody = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#blockBody.
HandlebarsParserListener.prototype.exitBlockBody = function(ctx) {
};


// Enter a parse tree produced by HandlebarsParser#closeBlock.
HandlebarsParserListener.prototype.enterCloseBlock = function(ctx) {
};

// Exit a parse tree produced by HandlebarsParser#closeBlock.
HandlebarsParserListener.prototype.exitCloseBlock = function(ctx) {
};



exports.HandlebarsParserListener = HandlebarsParserListener;