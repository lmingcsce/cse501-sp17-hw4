const antlr4 = require('antlr4');
const HandlebarsLexer = require('./HandlebarsLexer').HandlebarsLexer;
const HandlebarsParser = require('./HandlebarsParser').HandlebarsParser;
const HandlebarsParserListener = require('./HandlebarsParserListener').HandlebarsParserListener;

function HandlebarsCompiler() {
    HandlebarsParserListener.call(this);
    this._inputVar = "__$ctx";
    this._outputVar = "__$result";
    this._helpers = { expr: {}, block: {} };
    this._usedHelpers = [];

    return this;
}

HandlebarsCompiler.prototype = Object.create(HandlebarsParserListener.prototype);
HandlebarsCompiler.prototype.constructor = HandlebarsCompiler;

HandlebarsCompiler.escape = function (string) {
    return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (c) {
        switch (c) {
            case '"':
            case "'":
            case '\\':
                return '\\' + c;
            case '\n':
                return '\\n';
            case '\r':
                return '\\r';
            case '\u2028':
                return '\\u2028';
            case '\u2029':
                return '\\u2029';
        }
    })
};

HandlebarsCompiler.prototype.registerExprHelper = function(name, helper) {
    this._helpers.expr[name] = helper;
};

HandlebarsCompiler.prototype.registerBlockHelper = function (name, helper) {
    this._helpers.block[name] = helper;
};

HandlebarsCompiler.prototype.compile = function (template) {
    this._bodySource = `var ${this._outputVar} = "";\n`;

    const chars = new antlr4.InputStream(template);
    const lexer = new HandlebarsLexer(chars);
    const tokens = new antlr4.CommonTokenStream(lexer);
    const parser = new HandlebarsParser(tokens);
    parser.buildParseTrees = true;
    const tree = parser.document();

    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

    for (var x of this._usedHelpers) {
        var func_str = this._helpers.expr[x.substring(6)].toString();
        this._bodySource = `var ${x} = ${func_str}\n` + this._bodySource;
    }

    this._bodySource += `return ${this._outputVar};\n`;

    for (var x in this._usedHelpers) {
        this._usedHelpers.pop();
    }


    return new Function(this._inputVar, this._bodySource);
};

HandlebarsCompiler.prototype.append = function (expr) {
    this._bodySource += `${this._outputVar} += ${expr};\n`
};

HandlebarsCompiler.prototype.exitRawElement = function (ctx) {
    this.append(`"${HandlebarsCompiler.escape(ctx.getText())}"`);
};

HandlebarsCompiler.prototype.exitLiteralExpressionElement = function (ctx) {
    this.append(ctx.literal.text);
};

HandlebarsCompiler.prototype.exitIdExpressionElement = function (ctx) {
    this.append(this._inputVar + '.' + ctx.id.text);
};

HandlebarsCompiler.prototype.enterHelperExpressionElement = function (ctx) {
    var flag = false;

    for (var x of this._usedHelpers) {
        if (x == ('mgliu_' + ctx.id.text)) {
            flag = true;
        }
    }

    if (!flag)
        this._usedHelpers.push('mgliu_' + ctx.id.text);
};

HandlebarsCompiler.prototype.enterParenthesizedExpression = function (ctx) {
    var flag = false;

    for (var x of this._usedHelpers) {
        if (x == ('mgliu_' + ctx.id.text)) {
            flag = true;
        }
    }

    if (!flag)
        this._usedHelpers.push('mgliu_' + ctx.id.text);
};

HandlebarsCompiler.prototype.exitParenthesizedExpression =  function (ctx) {
    var fun_str = "";
    
    fun_str += 'mgliu_' + ctx.id.text;
    fun_str += '(';
    fun_str += this._inputVar;
    for (i = 2; i < (ctx.children.length - 1); i++) {
        fun_str += ', ';
        if (ctx.children[i].source == null) {
            if (ctx.children[i].getSymbol().type == 6) {
                fun_str += this._inputVar + '.' + ctx.children[i].getSymbol().text;
            } else {
                fun_str += ctx.children[i].getSymbol().text;
            }
        } else {
            fun_str += ctx.children[i].source;
        }
    }
    fun_str += ')';

    ctx.source = fun_str;
};

HandlebarsParserListener.prototype.exitParenthesizedExpressionElement = function(ctx) {
    var fun_str = "";

    if (ctx.children[1].source == null) {
        if (ctx.children[1].getSymbol().type == 6) {
            fun_str += this._inputVar + '.' + ctx.children[1].getSymbol().text;
        } else {
            fun_str += ctx.children[1].getSymbol().text;
        }
    } else {
        fun_str += ctx.children[1].source;
    }

    this.append(fun_str);
};

HandlebarsCompiler.prototype.exitHelperExpressionElement = function (ctx) {
    var fun_str = "";
    
    fun_str += 'mgliu_' + ctx.id.text;
    fun_str += '(';
    fun_str += this._inputVar;
    for (i = 2; i < (ctx.children.length - 1); i++) {
        fun_str += ', ';
        if (ctx.children[i].source == null) {
            if (ctx.children[i].getSymbol().type == 6) {
                fun_str += this._inputVar + '.' + ctx.children[i].getSymbol().text;
            } else {
                fun_str += ctx.children[i].getSymbol().text;
            }
        } else {
            fun_str += ctx.children[i].source;
        }
    }
    fun_str += ')';

    this.append(fun_str);
};

exports.HandlebarsCompiler = HandlebarsCompiler;

