parser grammar HandlebarsParser;

options { language=JavaScript; tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement 
    : TEXT (TEXT | BRACE)*
    ;

expressionElement
    : literalExpressionElement
    | idExpressionElement
    | parenthesizedExpressionElement
    | helperExpressionElement
    ;

literalExpressionElement 
    : START literal=(STRING | INTEGER | FLOAT) END 
    ;

idExpressionElement 
    : START id=ID END
    ;

parenthesizedExpression returns [source] :
    OPEN_PAREN id=ID (STRING | INTEGER | FLOAT | ID | parenthesizedExpression)+ CLOSE_PAREN 
    ;

parenthesizedExpressionElement 
    : START parenthesizedExpression END 
    ;

helperExpressionElement 
    : START id=ID (STRING | INTEGER | FLOAT | ID | parenthesizedExpression)+ END 
    ;

commentElement 
    : START COMMENT END_COMMENT 
    ;

blockElement
    : openBlock blockBody closeBlock
    ;

openBlock
    : START BLOCK blockHelper END
    ;

blockHelper
    : id=ID (STRING | INTEGER | FLOAT | ID | parenthesizedExpression)+ 
    ;

blockBody
    : (rawElement | expressionElement | commentElement)+
    ;

closeBlock
    : START CLOSE_BLOCK ID END
    ;

