// Generated from HandlebarsParser.g4 by ANTLR 4.7
// jshint ignore: start
var antlr4 = require('antlr4/index');
var HandlebarsParserListener = require('./HandlebarsParserListener').HandlebarsParserListener;
var grammarFileName = "HandlebarsParser.g4";

var serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
    "\u0003\u0012\u0085\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004",
    "\t\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007",
    "\u0004\b\t\b\u0004\t\t\t\u0004\n\t\n\u0004\u000b\t\u000b\u0004\f\t\f",
    "\u0004\r\t\r\u0004\u000e\t\u000e\u0004\u000f\t\u000f\u0004\u0010\t\u0010",
    "\u0003\u0002\u0007\u0002\"\n\u0002\f\u0002\u000e\u0002%\u000b\u0002",
    "\u0003\u0002\u0003\u0002\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003",
    "\u0005\u0003-\n\u0003\u0003\u0004\u0003\u0004\u0007\u00041\n\u0004\f",
    "\u0004\u000e\u00044\u000b\u0004\u0003\u0005\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0005\u0005:\n\u0005\u0003\u0006\u0003\u0006\u0003\u0006",
    "\u0003\u0006\u0003\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0003\b",
    "\u0003\b\u0003\b\u0003\b\u0003\b\u0003\b\u0003\b\u0006\bK\n\b\r\b\u000e",
    "\bL\u0003\b\u0003\b\u0003\t\u0003\t\u0003\t\u0003\t\u0003\n\u0003\n",
    "\u0003\n\u0003\n\u0003\n\u0003\n\u0003\n\u0006\n\\\n\n\r\n\u000e\n]",
    "\u0003\n\u0003\n\u0003\u000b\u0003\u000b\u0003\u000b\u0003\u000b\u0003",
    "\f\u0003\f\u0003\f\u0003\f\u0003\r\u0003\r\u0003\r\u0003\r\u0003\r\u0003",
    "\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0003\u000e\u0006",
    "\u000eu\n\u000e\r\u000e\u000e\u000ev\u0003\u000f\u0003\u000f\u0003\u000f",
    "\u0006\u000f|\n\u000f\r\u000f\u000e\u000f}\u0003\u0010\u0003\u0010\u0003",
    "\u0010\u0003\u0010\u0003\u0010\u0003\u0010\u0002\u0002\u0011\u0002\u0004",
    "\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018\u001a\u001c\u001e\u0002",
    "\u0004\u0003\u0002\u0003\u0004\u0003\u0002\u000e\u0010\u0002\u008f\u0002",
    "#\u0003\u0002\u0002\u0002\u0004,\u0003\u0002\u0002\u0002\u0006.\u0003",
    "\u0002\u0002\u0002\b9\u0003\u0002\u0002\u0002\n;\u0003\u0002\u0002\u0002",
    "\f?\u0003\u0002\u0002\u0002\u000eC\u0003\u0002\u0002\u0002\u0010P\u0003",
    "\u0002\u0002\u0002\u0012T\u0003\u0002\u0002\u0002\u0014a\u0003\u0002",
    "\u0002\u0002\u0016e\u0003\u0002\u0002\u0002\u0018i\u0003\u0002\u0002",
    "\u0002\u001an\u0003\u0002\u0002\u0002\u001c{\u0003\u0002\u0002\u0002",
    "\u001e\u007f\u0003\u0002\u0002\u0002 \"\u0005\u0004\u0003\u0002! \u0003",
    "\u0002\u0002\u0002\"%\u0003\u0002\u0002\u0002#!\u0003\u0002\u0002\u0002",
    "#$\u0003\u0002\u0002\u0002$&\u0003\u0002\u0002\u0002%#\u0003\u0002\u0002",
    "\u0002&\'\u0007\u0002\u0002\u0003\'\u0003\u0003\u0002\u0002\u0002(-",
    "\u0005\u0006\u0004\u0002)-\u0005\b\u0005\u0002*-\u0005\u0016\f\u0002",
    "+-\u0005\u0014\u000b\u0002,(\u0003\u0002\u0002\u0002,)\u0003\u0002\u0002",
    "\u0002,*\u0003\u0002\u0002\u0002,+\u0003\u0002\u0002\u0002-\u0005\u0003",
    "\u0002\u0002\u0002.2\u0007\u0003\u0002\u0002/1\t\u0002\u0002\u00020",
    "/\u0003\u0002\u0002\u000214\u0003\u0002\u0002\u000220\u0003\u0002\u0002",
    "\u000223\u0003\u0002\u0002\u00023\u0007\u0003\u0002\u0002\u000242\u0003",
    "\u0002\u0002\u00025:\u0005\n\u0006\u00026:\u0005\f\u0007\u00027:\u0005",
    "\u0010\t\u00028:\u0005\u0012\n\u000295\u0003\u0002\u0002\u000296\u0003",
    "\u0002\u0002\u000297\u0003\u0002\u0002\u000298\u0003\u0002\u0002\u0002",
    ":\t\u0003\u0002\u0002\u0002;<\u0007\u0005\u0002\u0002<=\t\u0003\u0002",
    "\u0002=>\u0007\u000b\u0002\u0002>\u000b\u0003\u0002\u0002\u0002?@\u0007",
    "\u0005\u0002\u0002@A\u0007\b\u0002\u0002AB\u0007\u000b\u0002\u0002B",
    "\r\u0003\u0002\u0002\u0002CD\u0007\f\u0002\u0002DJ\u0007\b\u0002\u0002",
    "EK\u0007\u0010\u0002\u0002FK\u0007\u000f\u0002\u0002GK\u0007\u000e\u0002",
    "\u0002HK\u0007\b\u0002\u0002IK\u0005\u000e\b\u0002JE\u0003\u0002\u0002",
    "\u0002JF\u0003\u0002\u0002\u0002JG\u0003\u0002\u0002\u0002JH\u0003\u0002",
    "\u0002\u0002JI\u0003\u0002\u0002\u0002KL\u0003\u0002\u0002\u0002LJ\u0003",
    "\u0002\u0002\u0002LM\u0003\u0002\u0002\u0002MN\u0003\u0002\u0002\u0002",
    "NO\u0007\r\u0002\u0002O\u000f\u0003\u0002\u0002\u0002PQ\u0007\u0005",
    "\u0002\u0002QR\u0005\u000e\b\u0002RS\u0007\u000b\u0002\u0002S\u0011",
    "\u0003\u0002\u0002\u0002TU\u0007\u0005\u0002\u0002U[\u0007\b\u0002\u0002",
    "V\\\u0007\u0010\u0002\u0002W\\\u0007\u000f\u0002\u0002X\\\u0007\u000e",
    "\u0002\u0002Y\\\u0007\b\u0002\u0002Z\\\u0005\u000e\b\u0002[V\u0003\u0002",
    "\u0002\u0002[W\u0003\u0002\u0002\u0002[X\u0003\u0002\u0002\u0002[Y\u0003",
    "\u0002\u0002\u0002[Z\u0003\u0002\u0002\u0002\\]\u0003\u0002\u0002\u0002",
    "][\u0003\u0002\u0002\u0002]^\u0003\u0002\u0002\u0002^_\u0003\u0002\u0002",
    "\u0002_`\u0007\u000b\u0002\u0002`\u0013\u0003\u0002\u0002\u0002ab\u0007",
    "\u0005\u0002\u0002bc\u0007\t\u0002\u0002cd\u0007\u0012\u0002\u0002d",
    "\u0015\u0003\u0002\u0002\u0002ef\u0005\u0018\r\u0002fg\u0005\u001c\u000f",
    "\u0002gh\u0005\u001e\u0010\u0002h\u0017\u0003\u0002\u0002\u0002ij\u0007",
    "\u0005\u0002\u0002jk\u0007\u0006\u0002\u0002kl\u0005\u001a\u000e\u0002",
    "lm\u0007\u000b\u0002\u0002m\u0019\u0003\u0002\u0002\u0002nt\u0007\b",
    "\u0002\u0002ou\u0007\u0010\u0002\u0002pu\u0007\u000f\u0002\u0002qu\u0007",
    "\u000e\u0002\u0002ru\u0007\b\u0002\u0002su\u0005\u000e\b\u0002to\u0003",
    "\u0002\u0002\u0002tp\u0003\u0002\u0002\u0002tq\u0003\u0002\u0002\u0002",
    "tr\u0003\u0002\u0002\u0002ts\u0003\u0002\u0002\u0002uv\u0003\u0002\u0002",
    "\u0002vt\u0003\u0002\u0002\u0002vw\u0003\u0002\u0002\u0002w\u001b\u0003",
    "\u0002\u0002\u0002x|\u0005\u0006\u0004\u0002y|\u0005\b\u0005\u0002z",
    "|\u0005\u0014\u000b\u0002{x\u0003\u0002\u0002\u0002{y\u0003\u0002\u0002",
    "\u0002{z\u0003\u0002\u0002\u0002|}\u0003\u0002\u0002\u0002}{\u0003\u0002",
    "\u0002\u0002}~\u0003\u0002\u0002\u0002~\u001d\u0003\u0002\u0002\u0002",
    "\u007f\u0080\u0007\u0005\u0002\u0002\u0080\u0081\u0007\u0007\u0002\u0002",
    "\u0081\u0082\u0007\b\u0002\u0002\u0082\u0083\u0007\u000b\u0002\u0002",
    "\u0083\u001f\u0003\u0002\u0002\u0002\u000e#,29JL[]tv{}"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ null, null, "'{'", "'{{'", "'#'", "'/'", null, "'!--'", 
                     null, "'}}'", "'('", "')'", null, null, null, null, 
                     "'--}}'" ];

var symbolicNames = [ null, "TEXT", "BRACE", "START", "BLOCK", "CLOSE_BLOCK", 
                      "ID", "COMMENT", "WS", "END", "OPEN_PAREN", "CLOSE_PAREN", 
                      "FLOAT", "INTEGER", "STRING", "ANY", "END_COMMENT" ];

var ruleNames =  [ "document", "element", "rawElement", "expressionElement", 
                   "literalExpressionElement", "idExpressionElement", "parenthesizedExpression", 
                   "parenthesizedExpressionElement", "helperExpressionElement", 
                   "commentElement", "blockElement", "openBlock", "blockHelper", 
                   "blockBody", "closeBlock" ];

function HandlebarsParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

HandlebarsParser.prototype = Object.create(antlr4.Parser.prototype);
HandlebarsParser.prototype.constructor = HandlebarsParser;

Object.defineProperty(HandlebarsParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

HandlebarsParser.EOF = antlr4.Token.EOF;
HandlebarsParser.TEXT = 1;
HandlebarsParser.BRACE = 2;
HandlebarsParser.START = 3;
HandlebarsParser.BLOCK = 4;
HandlebarsParser.CLOSE_BLOCK = 5;
HandlebarsParser.ID = 6;
HandlebarsParser.COMMENT = 7;
HandlebarsParser.WS = 8;
HandlebarsParser.END = 9;
HandlebarsParser.OPEN_PAREN = 10;
HandlebarsParser.CLOSE_PAREN = 11;
HandlebarsParser.FLOAT = 12;
HandlebarsParser.INTEGER = 13;
HandlebarsParser.STRING = 14;
HandlebarsParser.ANY = 15;
HandlebarsParser.END_COMMENT = 16;

HandlebarsParser.RULE_document = 0;
HandlebarsParser.RULE_element = 1;
HandlebarsParser.RULE_rawElement = 2;
HandlebarsParser.RULE_expressionElement = 3;
HandlebarsParser.RULE_literalExpressionElement = 4;
HandlebarsParser.RULE_idExpressionElement = 5;
HandlebarsParser.RULE_parenthesizedExpression = 6;
HandlebarsParser.RULE_parenthesizedExpressionElement = 7;
HandlebarsParser.RULE_helperExpressionElement = 8;
HandlebarsParser.RULE_commentElement = 9;
HandlebarsParser.RULE_blockElement = 10;
HandlebarsParser.RULE_openBlock = 11;
HandlebarsParser.RULE_blockHelper = 12;
HandlebarsParser.RULE_blockBody = 13;
HandlebarsParser.RULE_closeBlock = 14;

function DocumentContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_document;
    return this;
}

DocumentContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
DocumentContext.prototype.constructor = DocumentContext;

DocumentContext.prototype.EOF = function() {
    return this.getToken(HandlebarsParser.EOF, 0);
};

DocumentContext.prototype.element = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ElementContext);
    } else {
        return this.getTypedRuleContext(ElementContext,i);
    }
};

DocumentContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterDocument(this);
	}
};

DocumentContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitDocument(this);
	}
};




HandlebarsParser.DocumentContext = DocumentContext;

HandlebarsParser.prototype.document = function() {

    var localctx = new DocumentContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, HandlebarsParser.RULE_document);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 33;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===HandlebarsParser.TEXT || _la===HandlebarsParser.START) {
            this.state = 30;
            this.element();
            this.state = 35;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 36;
        this.match(HandlebarsParser.EOF);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_element;
    return this;
}

ElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ElementContext.prototype.constructor = ElementContext;

ElementContext.prototype.rawElement = function() {
    return this.getTypedRuleContext(RawElementContext,0);
};

ElementContext.prototype.expressionElement = function() {
    return this.getTypedRuleContext(ExpressionElementContext,0);
};

ElementContext.prototype.blockElement = function() {
    return this.getTypedRuleContext(BlockElementContext,0);
};

ElementContext.prototype.commentElement = function() {
    return this.getTypedRuleContext(CommentElementContext,0);
};

ElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterElement(this);
	}
};

ElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitElement(this);
	}
};




HandlebarsParser.ElementContext = ElementContext;

HandlebarsParser.prototype.element = function() {

    var localctx = new ElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, HandlebarsParser.RULE_element);
    try {
        this.state = 42;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,1,this._ctx);
        switch(la_) {
        case 1:
            this.enterOuterAlt(localctx, 1);
            this.state = 38;
            this.rawElement();
            break;

        case 2:
            this.enterOuterAlt(localctx, 2);
            this.state = 39;
            this.expressionElement();
            break;

        case 3:
            this.enterOuterAlt(localctx, 3);
            this.state = 40;
            this.blockElement();
            break;

        case 4:
            this.enterOuterAlt(localctx, 4);
            this.state = 41;
            this.commentElement();
            break;

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function RawElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_rawElement;
    return this;
}

RawElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RawElementContext.prototype.constructor = RawElementContext;

RawElementContext.prototype.TEXT = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.TEXT);
    } else {
        return this.getToken(HandlebarsParser.TEXT, i);
    }
};


RawElementContext.prototype.BRACE = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.BRACE);
    } else {
        return this.getToken(HandlebarsParser.BRACE, i);
    }
};


RawElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterRawElement(this);
	}
};

RawElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitRawElement(this);
	}
};




HandlebarsParser.RawElementContext = RawElementContext;

HandlebarsParser.prototype.rawElement = function() {

    var localctx = new RawElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, HandlebarsParser.RULE_rawElement);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 44;
        this.match(HandlebarsParser.TEXT);
        this.state = 48;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,2,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 45;
                _la = this._input.LA(1);
                if(!(_la===HandlebarsParser.TEXT || _la===HandlebarsParser.BRACE)) {
                this._errHandler.recoverInline(this);
                }
                else {
                	this._errHandler.reportMatch(this);
                    this.consume();
                } 
            }
            this.state = 50;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,2,this._ctx);
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ExpressionElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_expressionElement;
    return this;
}

ExpressionElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ExpressionElementContext.prototype.constructor = ExpressionElementContext;

ExpressionElementContext.prototype.literalExpressionElement = function() {
    return this.getTypedRuleContext(LiteralExpressionElementContext,0);
};

ExpressionElementContext.prototype.idExpressionElement = function() {
    return this.getTypedRuleContext(IdExpressionElementContext,0);
};

ExpressionElementContext.prototype.parenthesizedExpressionElement = function() {
    return this.getTypedRuleContext(ParenthesizedExpressionElementContext,0);
};

ExpressionElementContext.prototype.helperExpressionElement = function() {
    return this.getTypedRuleContext(HelperExpressionElementContext,0);
};

ExpressionElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterExpressionElement(this);
	}
};

ExpressionElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitExpressionElement(this);
	}
};




HandlebarsParser.ExpressionElementContext = ExpressionElementContext;

HandlebarsParser.prototype.expressionElement = function() {

    var localctx = new ExpressionElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 6, HandlebarsParser.RULE_expressionElement);
    try {
        this.state = 55;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,3,this._ctx);
        switch(la_) {
        case 1:
            this.enterOuterAlt(localctx, 1);
            this.state = 51;
            this.literalExpressionElement();
            break;

        case 2:
            this.enterOuterAlt(localctx, 2);
            this.state = 52;
            this.idExpressionElement();
            break;

        case 3:
            this.enterOuterAlt(localctx, 3);
            this.state = 53;
            this.parenthesizedExpressionElement();
            break;

        case 4:
            this.enterOuterAlt(localctx, 4);
            this.state = 54;
            this.helperExpressionElement();
            break;

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function LiteralExpressionElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_literalExpressionElement;
    this.literal = null; // Token
    return this;
}

LiteralExpressionElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LiteralExpressionElementContext.prototype.constructor = LiteralExpressionElementContext;

LiteralExpressionElementContext.prototype.START = function() {
    return this.getToken(HandlebarsParser.START, 0);
};

LiteralExpressionElementContext.prototype.END = function() {
    return this.getToken(HandlebarsParser.END, 0);
};

LiteralExpressionElementContext.prototype.STRING = function() {
    return this.getToken(HandlebarsParser.STRING, 0);
};

LiteralExpressionElementContext.prototype.INTEGER = function() {
    return this.getToken(HandlebarsParser.INTEGER, 0);
};

LiteralExpressionElementContext.prototype.FLOAT = function() {
    return this.getToken(HandlebarsParser.FLOAT, 0);
};

LiteralExpressionElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterLiteralExpressionElement(this);
	}
};

LiteralExpressionElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitLiteralExpressionElement(this);
	}
};




HandlebarsParser.LiteralExpressionElementContext = LiteralExpressionElementContext;

HandlebarsParser.prototype.literalExpressionElement = function() {

    var localctx = new LiteralExpressionElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 8, HandlebarsParser.RULE_literalExpressionElement);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 57;
        this.match(HandlebarsParser.START);
        this.state = 58;
        localctx.literal = this._input.LT(1);
        _la = this._input.LA(1);
        if(!((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << HandlebarsParser.FLOAT) | (1 << HandlebarsParser.INTEGER) | (1 << HandlebarsParser.STRING))) !== 0))) {
            localctx.literal = this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 59;
        this.match(HandlebarsParser.END);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function IdExpressionElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_idExpressionElement;
    this.id = null; // Token
    return this;
}

IdExpressionElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
IdExpressionElementContext.prototype.constructor = IdExpressionElementContext;

IdExpressionElementContext.prototype.START = function() {
    return this.getToken(HandlebarsParser.START, 0);
};

IdExpressionElementContext.prototype.END = function() {
    return this.getToken(HandlebarsParser.END, 0);
};

IdExpressionElementContext.prototype.ID = function() {
    return this.getToken(HandlebarsParser.ID, 0);
};

IdExpressionElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterIdExpressionElement(this);
	}
};

IdExpressionElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitIdExpressionElement(this);
	}
};




HandlebarsParser.IdExpressionElementContext = IdExpressionElementContext;

HandlebarsParser.prototype.idExpressionElement = function() {

    var localctx = new IdExpressionElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 10, HandlebarsParser.RULE_idExpressionElement);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 61;
        this.match(HandlebarsParser.START);
        this.state = 62;
        localctx.id = this.match(HandlebarsParser.ID);
        this.state = 63;
        this.match(HandlebarsParser.END);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ParenthesizedExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_parenthesizedExpression;
    this.source = null
    this.id = null; // Token
    return this;
}

ParenthesizedExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ParenthesizedExpressionContext.prototype.constructor = ParenthesizedExpressionContext;

ParenthesizedExpressionContext.prototype.OPEN_PAREN = function() {
    return this.getToken(HandlebarsParser.OPEN_PAREN, 0);
};

ParenthesizedExpressionContext.prototype.CLOSE_PAREN = function() {
    return this.getToken(HandlebarsParser.CLOSE_PAREN, 0);
};

ParenthesizedExpressionContext.prototype.ID = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.ID);
    } else {
        return this.getToken(HandlebarsParser.ID, i);
    }
};


ParenthesizedExpressionContext.prototype.STRING = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.STRING);
    } else {
        return this.getToken(HandlebarsParser.STRING, i);
    }
};


ParenthesizedExpressionContext.prototype.INTEGER = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.INTEGER);
    } else {
        return this.getToken(HandlebarsParser.INTEGER, i);
    }
};


ParenthesizedExpressionContext.prototype.FLOAT = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.FLOAT);
    } else {
        return this.getToken(HandlebarsParser.FLOAT, i);
    }
};


ParenthesizedExpressionContext.prototype.parenthesizedExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ParenthesizedExpressionContext);
    } else {
        return this.getTypedRuleContext(ParenthesizedExpressionContext,i);
    }
};

ParenthesizedExpressionContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterParenthesizedExpression(this);
	}
};

ParenthesizedExpressionContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitParenthesizedExpression(this);
	}
};




HandlebarsParser.ParenthesizedExpressionContext = ParenthesizedExpressionContext;

HandlebarsParser.prototype.parenthesizedExpression = function() {

    var localctx = new ParenthesizedExpressionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 12, HandlebarsParser.RULE_parenthesizedExpression);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 65;
        this.match(HandlebarsParser.OPEN_PAREN);
        this.state = 66;
        localctx.id = this.match(HandlebarsParser.ID);
        this.state = 72; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 72;
            this._errHandler.sync(this);
            switch(this._input.LA(1)) {
            case HandlebarsParser.STRING:
                this.state = 67;
                this.match(HandlebarsParser.STRING);
                break;
            case HandlebarsParser.INTEGER:
                this.state = 68;
                this.match(HandlebarsParser.INTEGER);
                break;
            case HandlebarsParser.FLOAT:
                this.state = 69;
                this.match(HandlebarsParser.FLOAT);
                break;
            case HandlebarsParser.ID:
                this.state = 70;
                this.match(HandlebarsParser.ID);
                break;
            case HandlebarsParser.OPEN_PAREN:
                this.state = 71;
                this.parenthesizedExpression();
                break;
            default:
                throw new antlr4.error.NoViableAltException(this);
            }
            this.state = 74; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << HandlebarsParser.ID) | (1 << HandlebarsParser.OPEN_PAREN) | (1 << HandlebarsParser.FLOAT) | (1 << HandlebarsParser.INTEGER) | (1 << HandlebarsParser.STRING))) !== 0));
        this.state = 76;
        this.match(HandlebarsParser.CLOSE_PAREN);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ParenthesizedExpressionElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_parenthesizedExpressionElement;
    return this;
}

ParenthesizedExpressionElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ParenthesizedExpressionElementContext.prototype.constructor = ParenthesizedExpressionElementContext;

ParenthesizedExpressionElementContext.prototype.START = function() {
    return this.getToken(HandlebarsParser.START, 0);
};

ParenthesizedExpressionElementContext.prototype.parenthesizedExpression = function() {
    return this.getTypedRuleContext(ParenthesizedExpressionContext,0);
};

ParenthesizedExpressionElementContext.prototype.END = function() {
    return this.getToken(HandlebarsParser.END, 0);
};

ParenthesizedExpressionElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterParenthesizedExpressionElement(this);
	}
};

ParenthesizedExpressionElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitParenthesizedExpressionElement(this);
	}
};




HandlebarsParser.ParenthesizedExpressionElementContext = ParenthesizedExpressionElementContext;

HandlebarsParser.prototype.parenthesizedExpressionElement = function() {

    var localctx = new ParenthesizedExpressionElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 14, HandlebarsParser.RULE_parenthesizedExpressionElement);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 78;
        this.match(HandlebarsParser.START);
        this.state = 79;
        this.parenthesizedExpression();
        this.state = 80;
        this.match(HandlebarsParser.END);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function HelperExpressionElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_helperExpressionElement;
    this.id = null; // Token
    return this;
}

HelperExpressionElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
HelperExpressionElementContext.prototype.constructor = HelperExpressionElementContext;

HelperExpressionElementContext.prototype.START = function() {
    return this.getToken(HandlebarsParser.START, 0);
};

HelperExpressionElementContext.prototype.END = function() {
    return this.getToken(HandlebarsParser.END, 0);
};

HelperExpressionElementContext.prototype.ID = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.ID);
    } else {
        return this.getToken(HandlebarsParser.ID, i);
    }
};


HelperExpressionElementContext.prototype.STRING = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.STRING);
    } else {
        return this.getToken(HandlebarsParser.STRING, i);
    }
};


HelperExpressionElementContext.prototype.INTEGER = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.INTEGER);
    } else {
        return this.getToken(HandlebarsParser.INTEGER, i);
    }
};


HelperExpressionElementContext.prototype.FLOAT = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.FLOAT);
    } else {
        return this.getToken(HandlebarsParser.FLOAT, i);
    }
};


HelperExpressionElementContext.prototype.parenthesizedExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ParenthesizedExpressionContext);
    } else {
        return this.getTypedRuleContext(ParenthesizedExpressionContext,i);
    }
};

HelperExpressionElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterHelperExpressionElement(this);
	}
};

HelperExpressionElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitHelperExpressionElement(this);
	}
};




HandlebarsParser.HelperExpressionElementContext = HelperExpressionElementContext;

HandlebarsParser.prototype.helperExpressionElement = function() {

    var localctx = new HelperExpressionElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 16, HandlebarsParser.RULE_helperExpressionElement);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 82;
        this.match(HandlebarsParser.START);
        this.state = 83;
        localctx.id = this.match(HandlebarsParser.ID);
        this.state = 89; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 89;
            this._errHandler.sync(this);
            switch(this._input.LA(1)) {
            case HandlebarsParser.STRING:
                this.state = 84;
                this.match(HandlebarsParser.STRING);
                break;
            case HandlebarsParser.INTEGER:
                this.state = 85;
                this.match(HandlebarsParser.INTEGER);
                break;
            case HandlebarsParser.FLOAT:
                this.state = 86;
                this.match(HandlebarsParser.FLOAT);
                break;
            case HandlebarsParser.ID:
                this.state = 87;
                this.match(HandlebarsParser.ID);
                break;
            case HandlebarsParser.OPEN_PAREN:
                this.state = 88;
                this.parenthesizedExpression();
                break;
            default:
                throw new antlr4.error.NoViableAltException(this);
            }
            this.state = 91; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << HandlebarsParser.ID) | (1 << HandlebarsParser.OPEN_PAREN) | (1 << HandlebarsParser.FLOAT) | (1 << HandlebarsParser.INTEGER) | (1 << HandlebarsParser.STRING))) !== 0));
        this.state = 93;
        this.match(HandlebarsParser.END);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CommentElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_commentElement;
    return this;
}

CommentElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CommentElementContext.prototype.constructor = CommentElementContext;

CommentElementContext.prototype.START = function() {
    return this.getToken(HandlebarsParser.START, 0);
};

CommentElementContext.prototype.COMMENT = function() {
    return this.getToken(HandlebarsParser.COMMENT, 0);
};

CommentElementContext.prototype.END_COMMENT = function() {
    return this.getToken(HandlebarsParser.END_COMMENT, 0);
};

CommentElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterCommentElement(this);
	}
};

CommentElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitCommentElement(this);
	}
};




HandlebarsParser.CommentElementContext = CommentElementContext;

HandlebarsParser.prototype.commentElement = function() {

    var localctx = new CommentElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 18, HandlebarsParser.RULE_commentElement);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 95;
        this.match(HandlebarsParser.START);
        this.state = 96;
        this.match(HandlebarsParser.COMMENT);
        this.state = 97;
        this.match(HandlebarsParser.END_COMMENT);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function BlockElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_blockElement;
    return this;
}

BlockElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
BlockElementContext.prototype.constructor = BlockElementContext;

BlockElementContext.prototype.openBlock = function() {
    return this.getTypedRuleContext(OpenBlockContext,0);
};

BlockElementContext.prototype.blockBody = function() {
    return this.getTypedRuleContext(BlockBodyContext,0);
};

BlockElementContext.prototype.closeBlock = function() {
    return this.getTypedRuleContext(CloseBlockContext,0);
};

BlockElementContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterBlockElement(this);
	}
};

BlockElementContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitBlockElement(this);
	}
};




HandlebarsParser.BlockElementContext = BlockElementContext;

HandlebarsParser.prototype.blockElement = function() {

    var localctx = new BlockElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 20, HandlebarsParser.RULE_blockElement);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 99;
        this.openBlock();
        this.state = 100;
        this.blockBody();
        this.state = 101;
        this.closeBlock();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function OpenBlockContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_openBlock;
    return this;
}

OpenBlockContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
OpenBlockContext.prototype.constructor = OpenBlockContext;

OpenBlockContext.prototype.START = function() {
    return this.getToken(HandlebarsParser.START, 0);
};

OpenBlockContext.prototype.BLOCK = function() {
    return this.getToken(HandlebarsParser.BLOCK, 0);
};

OpenBlockContext.prototype.blockHelper = function() {
    return this.getTypedRuleContext(BlockHelperContext,0);
};

OpenBlockContext.prototype.END = function() {
    return this.getToken(HandlebarsParser.END, 0);
};

OpenBlockContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterOpenBlock(this);
	}
};

OpenBlockContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitOpenBlock(this);
	}
};




HandlebarsParser.OpenBlockContext = OpenBlockContext;

HandlebarsParser.prototype.openBlock = function() {

    var localctx = new OpenBlockContext(this, this._ctx, this.state);
    this.enterRule(localctx, 22, HandlebarsParser.RULE_openBlock);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 103;
        this.match(HandlebarsParser.START);
        this.state = 104;
        this.match(HandlebarsParser.BLOCK);
        this.state = 105;
        this.blockHelper();
        this.state = 106;
        this.match(HandlebarsParser.END);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function BlockHelperContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_blockHelper;
    this.id = null; // Token
    return this;
}

BlockHelperContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
BlockHelperContext.prototype.constructor = BlockHelperContext;

BlockHelperContext.prototype.ID = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.ID);
    } else {
        return this.getToken(HandlebarsParser.ID, i);
    }
};


BlockHelperContext.prototype.STRING = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.STRING);
    } else {
        return this.getToken(HandlebarsParser.STRING, i);
    }
};


BlockHelperContext.prototype.INTEGER = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.INTEGER);
    } else {
        return this.getToken(HandlebarsParser.INTEGER, i);
    }
};


BlockHelperContext.prototype.FLOAT = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(HandlebarsParser.FLOAT);
    } else {
        return this.getToken(HandlebarsParser.FLOAT, i);
    }
};


BlockHelperContext.prototype.parenthesizedExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ParenthesizedExpressionContext);
    } else {
        return this.getTypedRuleContext(ParenthesizedExpressionContext,i);
    }
};

BlockHelperContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterBlockHelper(this);
	}
};

BlockHelperContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitBlockHelper(this);
	}
};




HandlebarsParser.BlockHelperContext = BlockHelperContext;

HandlebarsParser.prototype.blockHelper = function() {

    var localctx = new BlockHelperContext(this, this._ctx, this.state);
    this.enterRule(localctx, 24, HandlebarsParser.RULE_blockHelper);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 108;
        localctx.id = this.match(HandlebarsParser.ID);
        this.state = 114; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 114;
            this._errHandler.sync(this);
            switch(this._input.LA(1)) {
            case HandlebarsParser.STRING:
                this.state = 109;
                this.match(HandlebarsParser.STRING);
                break;
            case HandlebarsParser.INTEGER:
                this.state = 110;
                this.match(HandlebarsParser.INTEGER);
                break;
            case HandlebarsParser.FLOAT:
                this.state = 111;
                this.match(HandlebarsParser.FLOAT);
                break;
            case HandlebarsParser.ID:
                this.state = 112;
                this.match(HandlebarsParser.ID);
                break;
            case HandlebarsParser.OPEN_PAREN:
                this.state = 113;
                this.parenthesizedExpression();
                break;
            default:
                throw new antlr4.error.NoViableAltException(this);
            }
            this.state = 116; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << HandlebarsParser.ID) | (1 << HandlebarsParser.OPEN_PAREN) | (1 << HandlebarsParser.FLOAT) | (1 << HandlebarsParser.INTEGER) | (1 << HandlebarsParser.STRING))) !== 0));
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function BlockBodyContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_blockBody;
    return this;
}

BlockBodyContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
BlockBodyContext.prototype.constructor = BlockBodyContext;

BlockBodyContext.prototype.rawElement = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(RawElementContext);
    } else {
        return this.getTypedRuleContext(RawElementContext,i);
    }
};

BlockBodyContext.prototype.expressionElement = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionElementContext);
    } else {
        return this.getTypedRuleContext(ExpressionElementContext,i);
    }
};

BlockBodyContext.prototype.commentElement = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(CommentElementContext);
    } else {
        return this.getTypedRuleContext(CommentElementContext,i);
    }
};

BlockBodyContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterBlockBody(this);
	}
};

BlockBodyContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitBlockBody(this);
	}
};




HandlebarsParser.BlockBodyContext = BlockBodyContext;

HandlebarsParser.prototype.blockBody = function() {

    var localctx = new BlockBodyContext(this, this._ctx, this.state);
    this.enterRule(localctx, 26, HandlebarsParser.RULE_blockBody);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 121; 
        this._errHandler.sync(this);
        var _alt = 1;
        do {
        	switch (_alt) {
        	case 1:
        		this.state = 121;
        		this._errHandler.sync(this);
        		var la_ = this._interp.adaptivePredict(this._input,10,this._ctx);
        		switch(la_) {
        		case 1:
        		    this.state = 118;
        		    this.rawElement();
        		    break;

        		case 2:
        		    this.state = 119;
        		    this.expressionElement();
        		    break;

        		case 3:
        		    this.state = 120;
        		    this.commentElement();
        		    break;

        		}
        		break;
        	default:
        		throw new antlr4.error.NoViableAltException(this);
        	}
        	this.state = 123; 
        	this._errHandler.sync(this);
        	_alt = this._interp.adaptivePredict(this._input,11, this._ctx);
        } while ( _alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER );
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CloseBlockContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = HandlebarsParser.RULE_closeBlock;
    return this;
}

CloseBlockContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CloseBlockContext.prototype.constructor = CloseBlockContext;

CloseBlockContext.prototype.START = function() {
    return this.getToken(HandlebarsParser.START, 0);
};

CloseBlockContext.prototype.CLOSE_BLOCK = function() {
    return this.getToken(HandlebarsParser.CLOSE_BLOCK, 0);
};

CloseBlockContext.prototype.ID = function() {
    return this.getToken(HandlebarsParser.ID, 0);
};

CloseBlockContext.prototype.END = function() {
    return this.getToken(HandlebarsParser.END, 0);
};

CloseBlockContext.prototype.enterRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.enterCloseBlock(this);
	}
};

CloseBlockContext.prototype.exitRule = function(listener) {
    if(listener instanceof HandlebarsParserListener ) {
        listener.exitCloseBlock(this);
	}
};




HandlebarsParser.CloseBlockContext = CloseBlockContext;

HandlebarsParser.prototype.closeBlock = function() {

    var localctx = new CloseBlockContext(this, this._ctx, this.state);
    this.enterRule(localctx, 28, HandlebarsParser.RULE_closeBlock);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 125;
        this.match(HandlebarsParser.START);
        this.state = 126;
        this.match(HandlebarsParser.CLOSE_BLOCK);
        this.state = 127;
        this.match(HandlebarsParser.ID);
        this.state = 128;
        this.match(HandlebarsParser.END);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


exports.HandlebarsParser = HandlebarsParser;
